"""
import math
import numpy as np
from scipy.integrate import quad


def function(x):
    return math.exp(-x)*x**((N_glob)-1)

def igamc(a,b):
    xmin = b
    xmax = 100
    numerator, err = quad(function, xmin, xmax)
    denominator = math.gamma(a)
    return numerator/denominator
"""



import random


from NIST_RNG_Test.functions import frequency_monobit_test
from NIST_RNG_Test.functions import runs_test
from NIST_RNG_Test.functions import universal_statistical_test
from NIST_RNG_Test.functions import block_frequency_test
from NIST_RNG_Test.functions import longest_run_of_ones_test
from NIST_RNG_Test.functions import rank_test
from NIST_RNG_Test.functions import discreteFourierTransform
from NIST_RNG_Test.functions import nonOverlappingTemplateMatching

from NIST_RNG_Test.functions import personal_mean
from NIST_RNG_Test.functions import personal_standardDeviation
from NIST_RNG_Test.functions import fileToList
from NIST_RNG_Test.functions import loopOnTest
from NIST_RNG_Test.functions import file_creation




"""
sequences_QRNG_ANU = fileToList("random_sequence_QRNG_ANU.txt")

mean, standard_deviation = loopOnTest(frequency_monobit_test, sequences_QRNG_ANU)
print(mean)
print(standard_deviation)
"""




















