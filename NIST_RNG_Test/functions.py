# author: JOUFFROY Léopold, ENGEL Étienne
# coding: utf-8


'''
The aim of this code is to determine if a sequence is random or not.
This code do so by following many of 15 process which could be find here:
https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-22r1a.pdf

This code put the several process into practice:
    the Frequency Monobit Test (1st one)
    the Runs Test (3th one)

User only have to use the frequency_monobit_test.
The other functions have been calibrated by the nist examples to produce same output for same input
'''


import random
import secrets
import math
import numpy as np
from scipy.integrate import quad
from scipy.stats import norm





def personal_mean(l):
    '''
    Aim:
        Calculates the mean of a list of numbers
    
    Parameters:
        l (list): A list of number.
    
    Output:
        res (float): The mean of the list of number.
    '''
    s = 0
    for k in range(0, len(l)):
        s = s+l[k]
    return (s/len(l))

def personal_var(l):
    '''
    Aim:
        Calculates the variance of a list of numbers
    
    Parameters:
        l (list): A list of number.
    
    Output:
        res (float): The variance of the list of number.
    '''
    m = personal_mean(l)
    v = 0
    for k in range(0, len(l)):
        v = v+(l[k]-m)**2
    return (v/len(l))

def personal_standardDeviation(l):
    '''
    Aim:
        Calculates the standard deviation of a list of numbers.
        It measures the average deviation of the values from their mean.
    
    Parameters:
        l (list): A list of number.
    
    Output:
        res (float): The variance of the list of number.
    '''
    var = personal_var(l)
    return math.sqrt(var)





def file_creation(filename, modulename, number_of_random_sequences, size_of_random_sequences):
    '''
    Aim:
        Create a file containing several PRNG sequences
    
    Parameters:
        filename (str): The name of the file to create
        modulename (str): The name of the module used to generate the random sequence
        number_of_random_sequences (int): The number of PRNG sequences to add to the file
        size_of_random_sequences (int): The size of each PRNG sequence
    
    Output:

    '''
    for i in range(number_of_random_sequences):
        if (modulename == "random"):
            data_PRNG = random.choices([0,1], k=size_of_random_sequences)
            file_data_PRNG = ""
            for val in data_PRNG:
                file_data_PRNG += str(val)
            if (i == 0):
                with open(filename, "w") as file:
                    file.write(file_data_PRNG)
            else:
                with open(filename, "a") as file:
                    file.write('\n')
                    file.write('\n')
                    file.write(file_data_PRNG)
        elif (modulename == "secrets"):
            data_PRNG = []
            for j in range(size_of_random_sequences):
                bit = secrets.choice([0,1])
                data_PRNG.append(bit)
            file_data_PRNG = ""
            for val in data_PRNG:
                file_data_PRNG += str(val)
            if (i == 0):
                with open(filename, "w") as file:
                    file.write(file_data_PRNG)
            else:
                with open(filename, "a") as file:
                    file.write('\n')
                    file.write('\n')
                    file.write(file_data_PRNG)
            

def fileToList(filename):
    '''
    Aim:
        Take each line of a file,
        Convert it into a list of int,
        Append this list of int in a global list
    
    Parameters:
        filename (str): The name of the file with its extension
    
    Output:
        one_line_one_list (list): The list which each value is a line converted in list of int
    '''
    one_line_one_list = []
    with open(filename) as file:
        for line in file :
            line = line.strip()
            if (len(line) != 1024):
                continue
            line = list(line)
            for i, val in enumerate(line):
                val = int(val)
                line.pop(i)
                line.insert(i, val)
            one_line_one_list.append(line)
    return one_line_one_list





def loopOnTest(testname, random_sequences):
    '''
    Aim:
        Compute the same NIST test on different list
        Return the mean and the standard deviation on Pvalue
    
    Parameters:
        testname (str): The name of the NIST test to execute
        random_sequences (list): The list which each value is a random sequence
    
    Output:
        mean (float): The Pvalue mean of the NIST test on each random sequence
        standard_deviation (float): The Pvalue standard_deviation of the NIST test on each random sequence
    '''
    Pvalue_list = []
    for val in random_sequences:
        if (testname == "frequency_monobit_test"):
            Pvalue, decision = frequency_monobit_test(val)
            Pvalue_list.append(Pvalue)
        elif (testname == "block_frequency_test"):
            Pvalue, decision = block_frequency_test(20, val)
            Pvalue_list.append(Pvalue)
        elif (testname == "runs_test"):
            Pvalue, decision = runs_test(val)
            Pvalue_list.append(Pvalue)
        elif (testname == "longest_run_of_ones_test"):
            Pvalue, decision = longest_run_of_ones_test(3, 8, val)
            Pvalue_list.append(Pvalue)
        elif (testname == "rank_test"):
            Pvalue, decision = rank_test(32, 32, val)
            Pvalue_list.append(Pvalue)
        elif (testname == "nonOverlappingTemplateMatching"):
            Pvalue, decision = nonOverlappingTemplateMatching([0,0,0,0,0,0,0,0,1], val, 8)
            Pvalue_list.append(Pvalue)
        elif ( testname == "universal_statistical_test"):
            Pvalue, decision = universal_statistical_test(640, 6, val)
            Pvalue_list.append(Pvalue)

    mean = personal_mean(Pvalue_list)
    standard_deviation = personal_standardDeviation(Pvalue_list)

    return mean, standard_deviation





def function(x): 
    return math.exp(-x)*x**((N_glob)-1)

def igamc(a,b):
    xmin = b
    xmax = 100
    numerator, err = quad(function, xmin, xmax)
    denominator = math.gamma(a)
    return numerator/denominator

#test 1
def frequency_monobit_test(rand_list):
    '''
    Aim:
        Perform the Frequency Monobit Test
    
    Parameters:
        rand_list (list): A random list of 0 and 1. Minimum recommended length = 100
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''
    n = len(rand_list)
    Sn_classical = 0
    s_obs = 0
    Pvalue = 0

    for val in rand_list:
        if val == 1:
            Sn_classical += 1
        elif val == 0:
            Sn_classical -= 1

    s_obs = abs(Sn_classical) / math.sqrt(n)

    Pvalue = math.erfc(s_obs/math.sqrt(2))
    
    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False
    
    return Pvalue, res

#test 2
def block_frequency_test(M, rand_list):
    '''
    Aim:
        Perform the Frequency Test within a Block
    
    Parameters:
        M (int): The length of each block
        rand_list (list): A random list of 0 and 1. Minimum recommended length = 100
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''
    n = len(rand_list)
    
    N = math.floor(n/M)
    if (N >= 100):
        raise ValueError("N = math.floor(len(rand_list)/M) should be < 100")
    block = []
    for i in range(N):
        val = rand_list[i*M:i*M+M]
        block.append(val)

    ones_proportion = []
    for val in block:
        tmp = 0
        for i in range(M):
            if val[i] == 1:
                tmp += 1
        tmp = tmp/M
        ones_proportion.append(tmp)

    khi_square = 0
    for val in ones_proportion:
        khi_square += (val-0.5)**2
    khi_square = khi_square*4*M

    global N_glob
    N_glob = N/2
    Pvalue = igamc(N/2, khi_square/2)

    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False

    return Pvalue, res

#test 3
def runs_test(rand_list):
    '''
    Aim:
        Perform the runs test
    
    Parameters:
        rand_list (list): A random list of 0 and 1. Minimum recommended length = 100
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''
    n = len(rand_list)
    ones_count = 0
    for val in rand_list:
        if val == 1:
            ones_count += 1
    ones_proportion = ones_count / n

    if not frequency_monobit_test(rand_list):
        raise TypeError('random sequence does not pass frequency test')

    if not ( abs(ones_proportion-0.5)<=(2/math.sqrt(n)) ):
        raise TypeError('Prerequisite inegality not verified')

    Vn_obs = 0
    for i in range(n-1):
        if (rand_list[i] != rand_list[i+1]):
            Vn_obs += 1
    Vn_obs += 1

    Pvalue = math.erfc( abs(Vn_obs-2*n*ones_proportion*(1-ones_proportion)) / (2*math.sqrt(2*n)*ones_proportion*(1-ones_proportion)) )

    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False

    return Pvalue, res

#test 4
def longest_run_of_ones_test(K, M, rand_list):
    '''
    Aim:
        Perform the Longest Run of Ones in a Block Test
    
    Parameters:
        K (int): The number of different cases of longest run of one. K value is 3, 5 or 6
        M (int): The length of each block. M value is 8, 128 or 10000
        rand_list (list): A random list of 0 and 1.
            Minimum recommended length: 128 for M=8, 6272 for M=128, 750000 for M=10000
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''
    n = len(rand_list)

    if (M==8 and n<128):
        raise ValueError("Length of random list must be >= 128")
    if (M==128 and n<6272):
        raise ValueError("Length of random list must be >= 6272")
    if (M==10000 and n<750000):
        raise ValueError("Length of random list must be >= 750000")
    if (M==8 and K!=3):
        raise ValueError("Possible couple of (M,K) value are (8,3), (128,5) or (10000,6)")
    if (M==128 and K!=5):
        raise ValueError("Possible couple of (M,K) value are (8,3), (128,5) or (10000,6)")
    if (M==10000 and K!=6):
        raise ValueError("Possible couple of (M,K) value are (8,3), (128,5) or (10000,6)")
    if (n>=6272 and n<750000 and M!=128):
        print("Value of M should be 128")
    if (n>=750000 and M!=10000):
        print("Value of M sould be 10000")

    N = math.floor(n/M)
    block = []
    for i in range(N):
        val = rand_list[i*M:i*M+M]
        block.append(val)

    longest_run_of_ones = []
    for val in block:
        max_tmp = 0
        tmp = 0
        for i in range(M):
            if (val[i] == 1):
                tmp += 1
                if (i == M-1):
                    if (tmp > max_tmp):
                        max_tmp = tmp
                        tmp = 0
            elif (val[i] == 0):
                if (tmp > max_tmp):
                    max_tmp = tmp
                    tmp = 0
                else:
                    tmp = 0
        longest_run_of_ones.append(max_tmp)

    frequencies = []
    if (M == 8):
        nu0 = 0
        nu1 = 0
        nu2 = 0
        nu3 = 0
        for val in longest_run_of_ones:
            if (val<=1):
                nu0 += 1
            if (val==2):
                nu1 += 1
            if (val==3):
                nu2 += 1
            if (val>=4):
                nu3 += 1
        frequencies.append(nu0)
        frequencies.append(nu1)
        frequencies.append(nu2)
        frequencies.append(nu3)
    if (M == 128):
        nu0 = 0
        nu1 = 0
        nu2 = 0
        nu3 = 0
        nu4 = 0
        nu5 = 0
        for val in longest_run_of_ones:
            if (val<=4):
                nu0 += 1
            if (val==5):
                nu1 += 1
            if (val==6):
                nu2 += 1
            if (val==7):
                nu3 += 1
            if (val==8):
                nu4 += 1
            if (val>=9):
                nu5 += 1
        frequencies.append(nu0)
        frequencies.append(nu1)
        frequencies.append(nu2)
        frequencies.append(nu3)
        frequencies.append(nu4)
        frequencies.append(nu5)
    if (M == 10000):
        nu0 = 0
        nu1 = 0
        nu2 = 0
        nu3 = 0
        nu4 = 0
        nu5 = 0
        nu6 = 0
        for val in longest_run_of_ones:
            if (val<=10):
                nu0 += 1
            if (val==11):
                nu1 += 1
            if (val==12):
                nu2 += 1
            if (val==13):
                nu3 += 1
            if (val==14):
                nu4 += 1
            if (val==15):
                nu5 += 1
            if (val>=16):
                nu6 += 1
        frequencies.append(nu0)
        frequencies.append(nu1)
        frequencies.append(nu2)
        frequencies.append(nu3)
        frequencies.append(nu4)
        frequencies.append(nu5)
        frequencies.append(nu6)

    probabilities_pi = []
    if (K==3 and M==8):
        probabilities_pi.append(0.2148)
        probabilities_pi.append(0.3672)
        probabilities_pi.append(0.2305)
        probabilities_pi.append(0.1875)
    if (K==5 and M==128):
        probabilities_pi.append(0.1174)
        probabilities_pi.append(0.2430)
        probabilities_pi.append(0.2493)
        probabilities_pi.append(0.1752)
        probabilities_pi.append(0.1027)
        probabilities_pi.append(0.1124)
    if (K==6 and M==10000):
        probabilities_pi.append(0.0882)
        probabilities_pi.append(0.2092)
        probabilities_pi.append(0.2483)
        probabilities_pi.append(0.1933)
        probabilities_pi.append(0.1208)
        probabilities_pi.append(0.0675)
        probabilities_pi.append(0.0727)
    if (M==8 and K==3):
        N = 16
    if (M==128 and K==5):
        N = 49
    if (M==10000 and K==6):
        N = 75

    khi_square = 0
    for i in range(K+1):
        khi_square += ((frequencies[i]-N*probabilities_pi[i])**2) / (N*probabilities_pi[i])
    global N_glob
    N_glob = K/2
    Pvalue = igamc(K/2, khi_square/2)

    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False
    
    return Pvalue, res


#test 5
def rank_test(M, Q, rand_list):
    '''
    Aim:
        Perform the Binary Matrix Rank Test
    
    Parameters:
        M (int): The number of rows in each matrix
        Q (int): The number of columns in each matrix
        rand_list (list): A random list of 0 and 1. Minimum recommended length = 38*M*Q
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''
    n = len(rand_list)
    if (n < 38*M*Q):
        print("len(rand_list) shoud be >= 38*M*Q")
    N = math.floor(n/(M*Q))   
    matrice_list = []
    for i in range(N):
        matrice = []
        for j in range(M):
            matrice_row = rand_list[i*M*Q+j*Q:i*M*Q+j*Q+Q]
            matrice.append(matrice_row)
        matrice = np.array(matrice)
        matrice_list.append(matrice)

    R = []
    for i in range(N):
        rank = np.linalg.matrix_rank(matrice_list[i])
        R.append(rank)

    F_M = 0
    F_Mminus1 = 0
    for val in R:
        if (val == M):
            F_M += 1
        elif (val == M-1):
            F_Mminus1 += 1
    F_else = N-F_M-F_Mminus1

    khi_square = ((F_M - 0.2888*N)**2)/(0.2888*N) + ((F_Mminus1 - 0.5776*N)**2)/(0.5776*N) + ((F_else - 0.1336*N)**2)/(0.1336*N)

    global N_glob
    N_glob = 1
    Pvalue = igamc(1, khi_square/2)

    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False
    
    return Pvalue, res


#test 6
def discreteFourierTransform(array):
    
    n = len(array)
    print(n)

    for i,val in enumerate(array):
        if val == 0:
            array[i] = -1
    
    S = np.fft.fft(array)
    print(S)
    
    r = math.floor(len(S)/2)
    print(r)
    
    S = S[0:math.floor(len(S)/2)-1]
    print(S)
    
    M = abs(S)
    print(M)
    
    T = math.sqrt(math.log(1/0.05)*n)
    print(T)
    
    N0 = 0.95*(n/2)
    print(N0)
    
    N1 = 0
    for val in M:
        if val < T:
            N1 += 1
    print(N1)
    
    d = (N1-N0)/math.sqrt(n*0.95*0.05/4)
    print(d)
    
    Pvalue = math.erfc(abs(d)/math.sqrt(2))
    print(Pvalue)
    
    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False
    
    return Pvalue, res

#test 7
def nonOverlappingTemplateMatching(B, rand_list, N):
    '''
    Aim:
        Perform the Non-overlapping Template Matching Test
    
    Parameters:
        B (list): The template to be matched
        rand_list (list): A random list of 0 and 1.
        N (int): The number of independent blocks. N should be equal to 8
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''
    if (N!=8):
        print("N should be equals to 8")
    n = len(rand_list)
    M = math.floor(n/N)
    if (N != math.floor(n/M)):
        raise ValueError("M should be equals to len(input_list)/N")
    if (M <= 0.01*n):
        raise ValueError("M should be > 0.01*len(input_list)")
    block = []
    for i in range(N):
        tmp = []
        for j in range(M):
            tmp.append(rand_list[i*M+j])
        block.append(tmp)
    
    m = len(B)
    if (m < 9):
        print("Size of B should be 9 or 10. Maximum 10")
    W = []
    for val in block:
        cpt = 0
        flag = 0
        for i in range(M-m+1):
            tmp = val[i:i+m]
            if (flag==0):
                if (tmp==B):
                    cpt += 1
                    flag += m-1
            elif (flag>0):
                flag -= 1
        W.append(cpt)

    theoretical_mean = (M-m+1) / (2**m)
    theoretical_var = M * ((1/(2**m))-((2*m-1)/(2**(2*m))))

    khi_square = 0
    for i in range(N):
        khi_square += ((W[i]-theoretical_mean)**2) / theoretical_var

    global N_glob
    N_glob = N/2
    Pvalue = igamc(N/2, khi_square/2)

    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False
    
    return Pvalue, res




def dec2bin(d,nb=0):
    """
    dec2bin(d,nb=0):
        conversion nombre entier positif ou nul -> chaîne binaire
        (si nb>0, complète à gauche par des zéros)
    """
    if d==0:
        b="0"
    else:
        b=""
        while d!=0:
            b="01"[d&1]+b
            d=d>>1
    return b.zfill(nb)

#test 9
def universal_statistical_test(Q, L_input, rand_list):
    '''
    Aim:
        Perform the Maurer's "Universal Statistical" Test
    
    Parameters:
        L_input (int): Length of each block. Integers in [6;16]
        rand_list (list): A random list of 0 and 1. Minimum recommended length = (Q+K)*L
    
    Output:
        Pvalue (float): Goodness-of-Fit Distributional Test. 0 <= Pvalue <= 1.
        res (bool): True if the input is a random sequence and False if it's not.
    '''    
    n = len(rand_list)
    L = L_input
    K = math.floor(n/L)-Q

    if (L<6 or L>16):
        print("L must be an integer in [6;16]")
    if (n < ((Q+K)*L)):
        raise ValueError("n (length of rand_list) must be an integer >= (Q+K)*L")
    if (Q != 10*(2**L)):
        print("Q should be equal to 10 * 2**L")

    table_possible_Lbit_value = {}
    for i in range(2**L):
        table_possible_Lbit_value[str(dec2bin(i,L))] = 0
    for i in range(Q):
        val = rand_list[i*L:i*L+L]
        tmp = ''
        for nb in val:
            tmp += str(nb)
        table_possible_Lbit_value[tmp] = i+1

    sum_acc = 0
    for i in range(Q,Q+K):
        val = rand_list[i*L:i*L+L]
        tmp = ''
        for nb in val:
            tmp += str(nb)
        distance = i+1-table_possible_Lbit_value[tmp]
        table_possible_Lbit_value[tmp] = i+1
        sum_acc += math.log2(distance)

    f_n = sum_acc / K

    table_possible_variance = {'2':52.58622359, '6':2.954, '7':3.125, '8':3.238, '9':3.311, '10':3.356, '11':3.384, '12':3.401, '13':3.410, '14':3.416, '15':3.419, '16':3.421}
    table_possible_expectedValue = {'2':1.5374383, '6':5.2177052, '7':6.1962507, '8':7.1836656, '9':8.1764248, '10':9.1723243, '11':10.170032, '12':11.168765, '13':12.168070, '14':13.167693, '15':14.167488, '16':15.167379}
    c = 0.7-(0.8/L)+(4+(32/L))*(K**(-3/L)/15)
    sigma = c * math.sqrt(table_possible_variance[str(L)]/K)
    Pvalue = math.erfc( abs((f_n-table_possible_expectedValue[str(L)])/(math.sqrt(2)*sigma)) )
    
    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False

    return Pvalue, res

#test 13
def CumulativeSums(array):
    n = len(array)
    
    #normalize the array
    for i,val in enumerate(array):
        array[i] = 2*val-1
   
    print(array)
    
    #compute the partial sums
    S = []
    for i in range(n):
        tmp = 0
        for j in range(i+1):
            tmp += array[j]
        S.append(tmp)
    print(S)
    
    #find the max of S
    z = max(S)
    print(z)
    
    k1 = ((-n/z)+1)/4
    k2 = ((n/z)-1)/4
    k3 = ((-n/z)-3)/4
    
    Pvalue = 1 \
             - (norm.cdf(4*(k1+1)*z/math.sqrt(n)) - norm.cdf(4*(k1-1)*z/math.sqrt(n)))  \
             - (norm.cdf(4*(k2+1)*z/math.sqrt(n)) - norm.cdf(4*(k2-1)*z/math.sqrt(n)))  \
             + (norm.cdf(4*(k2+3)*z/math.sqrt(n)) - norm.cdf(4*(k2+1)*z/math.sqrt(n)))  \
             + (norm.cdf(4*(k3+3)*z/math.sqrt(n)) - norm.cdf(4*(k3+1)*z/math.sqrt(n)))
    
    if Pvalue >= 0.01:
        res = True
    if Pvalue < 0.01:
        res = False
    
    return res