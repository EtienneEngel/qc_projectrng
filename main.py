import random
import secrets


from NIST_RNG_Test.functions import frequency_monobit_test
from NIST_RNG_Test.functions import runs_test
from NIST_RNG_Test.functions import universal_statistical_test
from NIST_RNG_Test.functions import block_frequency_test
from NIST_RNG_Test.functions import longest_run_of_ones_test
from NIST_RNG_Test.functions import rank_test
from NIST_RNG_Test.functions import discreteFourierTransform
from NIST_RNG_Test.functions import nonOverlappingTemplateMatching

from NIST_RNG_Test.functions import personal_mean
from NIST_RNG_Test.functions import personal_standardDeviation
from NIST_RNG_Test.functions import fileToList
from NIST_RNG_Test.functions import loopOnTest
from NIST_RNG_Test.functions import file_creation




sequences_QRNG_ANU = fileToList("random_sequence_QRNG_ANU.txt")

mean, standard_deviation = loopOnTest("frequency_monobit_test", sequences_QRNG_ANU)
print(mean)
print(standard_deviation)

mean, standard_deviation = loopOnTest("block_frequency_test", sequences_QRNG_ANU)
print(mean)
print(standard_deviation)

mean, standard_deviation = loopOnTest("runs_test", sequences_QRNG_ANU)
print(mean)
print(standard_deviation)

mean, standard_deviation = loopOnTest("longest_run_of_ones_test", sequences_QRNG_ANU)
print(mean)
print(standard_deviation)

mean, standard_deviation = loopOnTest("rank_test", sequences_QRNG_ANU)
print(mean)
print(standard_deviation)

mean, standard_deviation = loopOnTest("nonOverlappingTemplateMatching", sequences_QRNG_ANU)
print(mean)
print(standard_deviation)




"""
#file_creation("random_sequence_PRNG.txt", "random", 10, 1024)
sequences_PRNG = fileToList("random_sequence_PRNG.txt")

mean, standard_deviation = loopOnTest(frequency_monobit_test, sequences_PRNG)
print(mean)
print(standard_deviation)




#file_creation("secrets_sequence_PRNG.txt", "secrets", 10, 1024)
sequences_PRNG = fileToList("secrets_sequence_PRNG.txt")

mean, standard_deviation = loopOnTest(frequency_monobit_test, sequences_PRNG)
print(mean)
print(standard_deviation)







sequences_QRNG_IBM = fileToList("random_sequence_QRNG_IBM.txt")
mean, standard_deviation = loopOnTest(frequency_monobit_test, sequences_QRNG_IBM)
print(mean)
print(standard_deviation)
"""