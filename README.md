# QC_ProjectRNG

## Context
This repository contains useful functions and processes to discuss if a sequence of 0 and 1 is random or not.

This repository puts into practice the tests set out in [this paper from NIST](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/nistspecialpublication800-22r1a.pdf)

## Explanation of contents

### Folder NIST_RNG_Test

----

The folder [NIST_RNG_Test](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/NIST_RNG_Test) contains:
1. the file [`__init__.py`](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/NIST_RNG_Test/__init__.py), so that python consider the folder as a package;
1. the file [`functions.py`](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/NIST_RNG_Test/functions.py), contains almost 1 function per NIST's test and other useful functions.

### File with .txt extension

----

They are used to store random sequences.
1 file .txt contains some random sequences of a same generation method.

### File basic_test.py

----

The file [`basic_test.py`](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/basic_test.py) contains the code to check the correct operation of each NIST test function.

The NIST provided for this purpose 1 or 2 examples per test function. When functions have the same inputs as the example they must have the same outputs as the example.

Just by runing `basic_test.py` if OK is displayed then all tested NIST test operate correctly.
Otherwise, FAILED is displayed with the number of failures and their localisation in the code.

![Unittest displayed OK](img/Capture2.png)

![Unittest displayed FAILED](img/Capture1.png)

### File main.py

----

The file [`main.py`](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/main.py) runs each NIST test on each random sequence of a file. Then it calculates for each NIST test, the mean and the standard deviation of the Pvalue values.

Run 55 times each test is the minimum recommended by the NIST.

### File temp.py

----

The file [`temp.py`](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/temp.py) is used for temporary test before release.

## Instructions to use the code of this repository

Before using the code of this repository, some package need to be installed.

In a linux terminal, write:
1. `pip install numpy`;
1. `pip install scipy`;

## Coming Soon

1. Rework of the file [`main.py`](https://gitlab.com/EtienneEngel/qc_projectrng/-/blob/master/main.py);
1. a virtual environment allowing instant use of the codes in this repository.
