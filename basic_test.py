import unittest

from NIST_RNG_Test.functions import frequency_monobit_test
from NIST_RNG_Test.functions import runs_test
from NIST_RNG_Test.functions import universal_statistical_test
from NIST_RNG_Test.functions import block_frequency_test
from NIST_RNG_Test.functions import longest_run_of_ones_test
from NIST_RNG_Test.functions import rank_test
from NIST_RNG_Test.functions import discreteFourierTransform
from NIST_RNG_Test.functions import nonOverlappingTemplateMatching



class Test(unittest.TestCase):
    """ Objective:
        Allow unitary test for the 15 NIST's test
    """

    def test_frequency_monobit_test(self):
        """ Objective:
            Test if Frequency Monobit Test works as NIST designed it
            NIST TEST #1
        """
        rand_list = [1,0,1,1,0,1,0,1,0,1]
        Pvalue, decision = frequency_monobit_test(rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.527089)

        rand_list = [1,1,0,0,1,0,0,1,0,0,0,0,1,1,1,1,1,1,0,1,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1,1,0,1,0,0,0,1,1,0,0,0,0,1,0,0,0,1,1,0,1,0,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,1,0,1,1,1,0,0,0]
        Pvalue, decision = frequency_monobit_test(rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.109599)

    def test_block_frequency_test(self):
        """ Objective:
            Test if Frequency Test within a Block works as NIST designed it
            NIST TEST #2
        """
        rand_list = [0,1,1,0,0,1,1,0,1,0]
        M = 3
        Pvalue, decision = block_frequency_test(M, rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.801252)

        rand_list = [1,1,0,0,1,0,0,1,0,0,0,0,1,1,1,1,1,1,0,1,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1,1,0,1,0,0,0,1,1,0,0,0,0,1,0,0,0,1,1,0,1,0,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,1,0,1,1,1,0,0,0]
        M = 10
        Pvalue, decision = block_frequency_test(M, rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.706438)

    def test_runs_test(self):
        """ Objective:
            Test if Runs Test works as NIST designed it
            NIST TEST #3
        """
        rand_list = [1,0,0,1,1,0,1,0,1,1]
        Pvalue, decision = runs_test(rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.147232)

        rand_list = [1,1,0,0,1,0,0,1,0,0,0,0,1,1,1,1,1,1,0,1,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1,1,0,1,0,0,0,1,1,0,0,0,0,1,0,0,0,1,1,0,1,0,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,1,0,1,1,1,0,0,0]
        Pvalue, decision = runs_test(rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.500798)     

    def test_longest_run_of_ones_test(self):
        """ Objective:
            Test if Longest Run of Ones in a Block Test works as NIST designed it
            NIST TEST #4
        """
        rand_list = [1,1,0,0,1,1,0,0,0,0,0,1,0,1,0,1,0,1,1,0,1,1,0,0,0,1,0,0,1,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,1,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,1,1,1,1,0,1,0,1,1,0,1,0,0,0,0,0,0,0,1,1,0,1,0,1,1,1,1,1,0,0,1,1,0,0,1,1,1,0,0,1,1,0,1,1,0,1,1,0,0,0,1,0,1,1,0,0,1,0]
        K = 3
        M = 8
        Pvalue, decision = longest_run_of_ones_test(K, M, rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.180598)

    def test_rank_test(self):
        """ Objective:
            Test if Binary Matrix Rank Test works as NIST designed it
            NIST TEST #5
        """       
        rand_list = [0,1,0,1,1,0,0,1,0,0,1,0,1,0,1,0,1,1,0,1]
        M = 3
        Q = 3
        Pvalue, decision = rank_test(M, Q, rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.741948)

    def test_discreteFourierTransform(self):
        """ Objective:
            Test if Discrete Fourier Transform Spectral Test works as NIST designed it
            NIST TEST #6
            NEED TO BE REWORK
        """       
        #rand_list = [1,0,0,1,0,1,0,0,1,1]
        #Pvalue, decision = discreteFourierTransform(rand_list)
        #Pvalue = round(Pvalue, 6)
        #self.assertEqual(Pvalue, 0.029523)

        #rand_list = [1,1,0,0,1,0,0,1,0,0,0,0,1,1,1,1,1,1,0,1,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1,1,0,1,0,0,0,1,1,0,0,0,0,1,0,0,0,1,1,0,1,0,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,1,0,1,1,1,0,0,0]
        #Pvalue, decision = discreteFourierTransform(rand_list)
        #Pvalue = round(Pvalue, 6)
        #self.assertEqual(Pvalue, 0.168669)

    def test_nonOverlappingTemplateMatching(self):
        """ Objective:
            Test if Non-overlapping Template Matching Test works as NIST designed it
            NIST TEST #7
        """       
        rand_list = [1,0,1,0,0,1,0,0,1,0,1,1,1,0,0,1,0,1,1,0]
        N = 2
        B = [0,0,1]
        Pvalue, decision = nonOverlappingTemplateMatching(B, rand_list, N)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.344154)

    def test_universal_statistical_test(self):
        """ Objective:
            Test if Maurer's Universal Statistical Test works as NIST designed it
            NIST TEST #9
        """
        rand_list = [0,1,0,1,1,0,1,0,0,1,1,1,0,1,0,1,0,1,1,1]
        Q = 4
        Pvalue, decision = universal_statistical_test(Q, 2, rand_list)
        Pvalue = round(Pvalue, 6)
        self.assertEqual(Pvalue, 0.767189)



if __name__ == "__main__":
    unittest.main()